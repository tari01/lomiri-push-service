Lomiri Push Service Client Low Level Developer Guide
====================================================

:Version: 0.50+

.. contents::

Introduction
------------

This document describes how to use the Push Client service from a platform integrator's point of view.
Application developers are expected to use a much simpler API, in turn based on the lower-level API described here.

The expected audience for this document is, therefore, either platform developers, or application developers who,
for whatever reason, can't use or prefer not to use the available higher level APIs.

---------

.. include:: _description.txt


The PushNotifications Service
-----------------------------

:Service: com.lomiri.PushNotifications
:Object path: /com/lomiri/PushNotifications/QUOTED_PKGNAME

The PushNotifications service handles registering the device with the Push service to enable delivery of messages to
it.

Each package has to use a separate object path for security reasons, that's why the object path includes QUOTED_PKGNAME.
For example, in the case of the music application, the package name is ``app.example`` and QUOTED_PKGNAME is ``app_2eexample``.
Everything that is not a letter or digit has to be quoted as _XX where XX are the hex digits of the character. In practice,
this means replacing "." with "_2e" and "-" with "_2d"

.. note:: For applications that are not installed as part of click packages, the QUOTED_PKGNAME is "_" and the APP_ID when required is
          _PACKAGENAME.

          For example, for lomiri-system-settings:

          * QUOTED_PKGNAME is _
          * APP_ID is _lomiri-system-settings


com.lomiri.PushNotifications.Register
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``string Register(string APP_ID)``

Example::

	$ gdbus call --session --dest com.lomiri.PushNotifications --object-path /com/lomiri/PushNotifications/app_2eexample \
	--method com.lomiri.PushNotifications.Register app.example_app

	('LeA4tRQG9hhEkuhngdouoA==',)

The Register method takes as argument the APP_ID (in the example,
app.example_app) and returns a token identifying the user and device.

The APP_ID is as described in the `ApplicationId documentation <https://wiki.ubuntu.com/AppStore/Interfaces/ApplicationId>`__
except that the version is treated as optional. Therefore both ``app.example_app`` and ``app.example_app_1.3.496``
are valid. Keep in mind that while both versioned and unversioned APP_IDs are valid, they are still different and will affect
which notifications are delivered to the application. Unversioned IDs mean the token will be the same after updates and the application
will receive old notifications, while versioned IDs mean the app needs to explicitly ask to get older messages delivered.

Register is idempotent, and calling it multiple times returns the same token.

This token is later used by the application server to indicate the recipient of notifications.

.. FIXME crosslink to server app

.. note:: There is currently no way to send a push message to all of a user's devices. The application server has to send to
          each registered device individually instead.

com.lomiri.PushNotifications.Unregister
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

``void Unregister(string APP_ID)``

Example::

	$ gdbus call --session --dest com.lomiri.PushNotifications --object-path /com/lomiri/PushNotifications/app_2eexample \
	--method com.lomiri.PushNotifications.Unregister app.example_app

The Unregister method invalidates the token obtained via `Register <#com-lomiri-pushnotifications-register>`_  therefore disabling
reception of push messages.

The method takes as argument the APP_ID (in the example, app.example_app) and returns nothing.

The APP_ID is as described in the `ApplicationId documentation <https://wiki.ubuntu.com/AppStore/Interfaces/ApplicationId>`__
except that the version is treated as optional. Therefore both ``app.example_app`` and ``app.example_app_1.3.496``
are valid.

The Postal Service
------------------

:Service: com.lomiri.Postal
:Object path: /com/lomiri/Postal/QUOTED_PKGNAME

The Postal service delivers the actual messages to the applications. After the application is registered, the push client will begin
delivering messages to the device, which will then (possibly) cause specific notifications to be presented to the user (message bubbles,
sounds, haptic feedbak, etc.) Regardless of whether the user acknowledges those notifications or not, the payload of the push message
is put in the Postal service for the application to pick up.

Because user response to notifications can cause application activation, apps should check the status of the Postal service every time
the application activates.

com.lomiri.Postal.Post
~~~~~~~~~~~~~~~~~~~~~~

``void Post(string APP_ID, string message)``

Example::

	gdbus call --session --dest com.lomiri.Postal --object-path /com/lomiri/Postal/app_2eexample \
	--method com.lomiri.Postal.Post app.example_app \
	'"{\"message\": \"foobar\", \"notification\":{\"card\": {\"summary\": \"yes\", \"body\": \"hello\", \"popup\": true, \"persist\": true}}}"'


The arguments for the Post method are APP_ID (in the example, app.example_app) and a JSON string
`describing a push message. <#helper-output-format>`__

Depending on the contents of the push message it may trigger user-facing notifications, and will queue a
message for the app to get via the `PopAll <#com-lomiri-postal-popalls>`__ method.

The APP_ID is as described in the `ApplicationId documentation <https://wiki.ubuntu.com/AppStore/Interfaces/ApplicationId>`__
except that the version is treated as optional. Therefore both ``app.example_app`` and ``app.example_app_1.3.496``
are valid.

.. note:: Post is useful as a unified frontend for notifications in Lomiri, since it wraps and abstracts several different APIs.

com.lomiri.Postal.PopAll
~~~~~~~~~~~~~~~~~~~~~~~~

``array{string} PopAll(string APP_ID)``

Example::

	$ gdbus call --session --dest com.lomiri.Postal --object-path /com/lomiri/Postal/app_2eexample \
	--method com.lomiri.Postal.PopAll app.example_app

	(['{"foo": "bar", ....}'],)

The argument for the PopAll method is the APP_ID and it returns a list of strings, each string being a separate postal
message, the "message" element of a helper's output fed from `Post <#com-lomiri-postal-post>`__
or from the Push service,

Post Signal
~~~~~~~~~~~

``void Post(string APP_ID)``

Every time a notification is posted, the postal service will emit the Post signal. Your app can connect to it to react to
incoming notifications if it's running when they arrive. Remember that on Lomiri, the application lifecycle means
it will often **not** be running when notifications arrive. If the application is in the foreground when a notification
arrives, the notification **will not** be presented.

The object path is similar to that of the Postal service methods, containing the QUOTED_PKGNAME.

Persistent Notification Management
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some notifications are persistent, meaning that, after they are presented, they don't disappear automatically.
This API allows the app to manage that type of notifications.

On each notification there's an optional ``tag`` field, used for this purpose.

``array(string) ListPersistent(string APP_ID)``

Returns a list of the tags of notifications with the "persist" element set to true that are visible to the user right now.

``void ClearPersistent(string APP_ID, [tag1, tag2,....])``

Clears persistent notifications for that app by tag(s). If none given, match all.

``void SetCounter(string APP_ID, int count int, bool visible)``

Set the counter to the given values.


.. include:: _common.txt
